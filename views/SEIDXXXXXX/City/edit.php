<?php
   require_once ("../../../vendor/autoload.php");
   if(!isset($_SESSION)) session_start();
   use App\Message\Message;
   use App\City\City;

   $obj = new City();
   $obj->setData($_GET);
   $oneData = $obj->view();

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>
        Atomic project
    </title>

    <link rel="stylesheet" href="../style.css">

    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

    <!-- required for search, block3 of 5 start -->

    <link rel="stylesheet" href="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.css">
    <script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
    <script src="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

    <!-- required for search, block3 of 5 end -->



</head>
<body background="../../../resources/images/atom9.png">

<div id="MessageShowDiv" style="height: 20px">
    <div id="message" class="btn-danger text-center">
        <?php
        if(isset($_SESSION['message'])){
            echo Message::message();
        }
        ?>
    </div>
</div>

<div style="text-align: center;font-size: xx-large;font-family: 'Lucida Calligraphy';color:#2098d1;background: rgba(0,0,0,0.5);padding-top: 30px;">
    <b>ATOMIC PROJECT</b>
    <br>

</div>


<nav class="navbar" style="font-family: 'Comic Sans MS'; background: rgba(0,0,0,0.5)" >
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a class="hvr-underline-from-center" href="../index.html">Home</a></li>
                <li><a class="hvr-underline-from-center" href="../BookTitle/index.php">Book Title</a></li>
                <li><a class="hvr-underline-from-center" href="../Birthday/index.php">Birthday</a></li>
                <li><a class="hvr-underline-from-center" href="../City/index.php">City</a></li>
                <li><a class="hvr-underline-from-center" href="../Email/index.php">Email</a></li>
                <li><a class="hvr-underline-from-center" href="../Gender/index.php">Gender</a></li>
                <li><a class="hvr-underline-from-center" href="../Hobbies/index.php">Hobbies</a></li>
                <li><a class="hvr-underline-from-center" href="../ProfilePicture/index.php">Profile Picture</a></li>
                <li><a class="hvr-underline-from-center" href="../Organization/index.php">Summary of organization</a></li>
            </ul>

            <form class="navbar-form" id="searchForm" action="index.php"  method="get">
                <div class="form-group" style="color:#FFF">
                    <input type="checkbox"  name="byName"   checked  >By Name
                    <input type="checkbox"  name="byEmail"  checked >By Email
                </div>
                <div class="input-group">
                    <input type="text" id="searchID" name="search" class="form-control" placeholder="Search">

                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>

            </form>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="navbar container">
    <a href='create.php' class='btn btn-lg bg-success'>Create</a>
    <a href='index.php' class='btn btn-lg bg-danger'>Active List</a>
    <a href='trashed.php' class='btn btn-lg bg-danger'>Trashed List</a>
</div>


<div class="container">


    <div class="col-sm-3"></div>
    <div class="col-lg-6" style="color: #2098d1; background:rgba(0,0,0,0.5); margin-top: 100px;margin-bottom: 150px; border-radius: 10px;padding-top: 10px;padding-bottom: 10px;font-family: 'Comic Sans MS'">


        <form action="update.php" method="post">
            <fieldset>
                <h2 style="text-align: center"><b> City - Edit Form </b></h2>
                <div class="form-group">
                    <label for="Name">Name</label>
                    <input type="text" class="form-control" id="Name" value="<?php echo $oneData->name ?>" placeholder="Name" name="Name">
                </div>
                <div class="form-group">
                    <label for="City">City</label>
                    <input type="text" class="form-control" id="City" value="<?php echo $oneData->city ?>" placeholder="City" name="City">
                </div>

                <div class="form-group">
                    <input type="hidden" name="id" value="<?php echo $oneData->id ?>"
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </fieldset>
        </form></div>

    <div class="col-sm-3"></div>


</div>



<script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>

<script>


    $(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    });




</script>


</body>
</html>