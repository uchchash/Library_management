<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;
use App\Utility\Utility;
use App\Email\Email;



$obj = new Email();

$allData  =  $obj->trashed();


################## search  block 1 of 5 start ##################
if(isset($_REQUEST['search']) )$someData =  $obj->searchTrashed($_REQUEST);
$availableKeywords=$obj->getAllKeywordsTrashed();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';
################## search  block 1 of 5 end ##################






######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;


$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 6;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ($recordCount<1)?1:ceil($recordCount/$itemsPerPage);
$someData = $obj->trashedPaginator($page,$itemsPerPage);

$allData = $someData;


$serial = (  ($page-1) * $itemsPerPage ) +1;



if($serial<1) $serial=1;

####################### pagination code block#1 of 2 end #########################################







################## search  block 2 of 5 start ##################

if(isset($_REQUEST['search']) ) {
    $someData = $obj->searchTrashed($_REQUEST);
    $allData = $someData;
    $serial = 1;
}
################## search  block 2 of 5 end ##################

?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>
        Atomic project
    </title>

    <link rel="stylesheet" href="../style.css">

    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

    <!-- required for search, block3 of 5 start -->

    <link rel="stylesheet" href="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.css">
    <script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
    <script src="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

    <!-- required for search, block3 of 5 end -->



</head>
<body background="../../../resources/images/atom9.png">

<div id="MessageShowDiv" style="height: 20px">
    <div id="message" class="btn-danger text-center">
        <?php
        if(isset($_SESSION['message'])){
            echo Message::message();
        }
        ?>
    </div>
</div>

<div style="text-align: center;font-size: xx-large;font-family: 'Lucida Calligraphy';color:#2098d1;background: rgba(0,0,0,0.5);padding-top: 30px;">
    <b>ATOMIC PROJECT</b>
    <br>

</div>


<nav class="navbar" style="font-family: 'Comic Sans MS'; background: rgba(0,0,0,0.5)" >
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a class="hvr-underline-from-center" href="../index.html">Home</a></li>
                <li><a class="hvr-underline-from-center" href="../BookTitle/index.php">Book Title</a></li>
                <li><a class="hvr-underline-from-center" href="../Birthday/index.php">Birthday</a></li>
                <li><a class="hvr-underline-from-center" href="../City/index.php">City</a></li>
                <li><a class="hvr-underline-from-center" href="../Email/index.php">Email</a></li>
                <li><a class="hvr-underline-from-center" href="../Gender/index.php">Gender</a></li>
                <li><a class="hvr-underline-from-center" href="../Hobbies/index.php">Hobbies</a></li>
                <li><a class="hvr-underline-from-center" href="../ProfilePicture/index.php">Profile Picture</a></li>
                <li><a class="hvr-underline-from-center" href="../Organization/index.php">Summary of organization</a></li>
            </ul>

            <form class="navbar-form" id="searchForm" action="trashed.php"  method="get">
                <div class="form-group" style="color:#FFF">
                    <input type="checkbox"  name="byName"   checked  >By Name
                    <input type="checkbox"  name="byDetails"  checked >By Details
                </div>
                <div class="input-group">
                    <input type="text" id="searchID" name="search" class="form-control" placeholder="Search">

                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>

            </form>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>


<div  class="container">





    <div class="navbar">
        <a href='create.php' class='btn btn-lg bg-success'>Create</a>
        <a href='index.php' class='btn btn-lg bg-danger'>Active List</a>


        <button id="RecoverSelected" class='btn btn-lg bg-danger'>Recover Selected</button>

        <button id="DeleteSelected" class='btn btn-lg bg-danger'>Delete Selected</button>


    </div>







    <div class="bg-info text-center" style="font-family: 'Comic Sans MS'; background: rgba(0,0,0,0.5);color:#2098d1;"><h1>Email - Trashed List</h1></div>

    <table border="1px" class="table table-bordered table-striped" style="font-family: 'Comic Sans MS'; background: rgba(0,0,0,0.5);color:#2098d1">
        <tr style="background: black">
            <th>Select all  <input id='select_all' type='checkbox' value='select all'></th>
            <th> Serial </th>
            <th> ID </th>
            <th> Name </th>
            <th> Email </th>
            <th> Action Buttons </th>

        </tr>

<form id="multiple" method="post">
        <?php


         $serial=1;

         foreach ($allData as $oneData){

             if($serial%2) $bgColor = "rbga(0,0,0,0.5)";
             else $bgColor = "rgba(0,0,0,0.5)";

             echo "
    
                                  <tr  style='background-color: $bgColor'>
    
                                     <td style='padding-left: 4%'><input type='checkbox' class='checkbox' name='mark[]' value='$oneData->id'></td>
    
                                     <td style='width: 10%; text-align: center'>$serial</td>
                                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                                     <td style='width: 20%;'>$oneData->name</td>
                                     <td>$oneData->email</td>
    
                                     <td>
                                       <a href='view.php?id=$oneData->id' class='btn btn-info'>View</a>
                                       <a href='edit.php?id=$oneData->id' class='btn btn-primary'>Edit</a>
                                       <a href='recover.php?id=$oneData->id' class='btn btn-success'>Recover</a>
                                       <a href='delete.php?id=$oneData->id'  onclick='return doConfirm()' class='btn btn-danger'>Delete</a>
                                       <a href='email.php?id=$oneData->id' class='btn btn-success'>Email</a>
    
                                     </td>
                                  </tr>
                              ";
             $serial++;

         }

       ?>
</form>
    </table>
<!--      ######################## pagination code block#2 of 2 start ######################################-->

    <?php
    if(!isset($_REQUEST['search'])){
    ?>

    <div align="left" class="container">
        <ul class="pagination">

            <?php

            $pageMinusOne  = $page-1;
            $pagePlusOne  = $page+1;


            if($page>$pages) Utility::redirect("trashed.php?Page=$pages");

            if($page>1)  echo "<li><a href='trashed.php?Page=$pageMinusOne'>" . "Previous" . "</a></li>";


            for($i=1;$i<=$pages;$i++)
            {
                if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
                else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

            }
            if($page<$pages) echo "<li><a href='trashed.php?Page=$pagePlusOne'>" . "Next" . "</a></li>";

            ?>

            <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
                <?php
                if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

                if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
                else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

                if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

                if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

                if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

                if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
                else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
                ?>
            </select>
        </ul>
    </div>
    <?php } ?>
<!--      ######################## pagination code block#2 of 2 end ######################################-->

</div>




<script>


    //select all checkboxes
    $("#select_all").change(function(){  //"select all" change
        var status = this.checked; // "select all" checked status
        $('.checkbox').each(function(){ //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
    });

    $('.checkbox').change(function(){ //".checkbox" change
//uncheck "select all", if one of the listed checkbox item is unchecked
        if(this.checked == false){ //if this item is unchecked
            $("#select_all")[0].checked = false; //change "select all" checked status to false
        }

//check "select all" if all checkbox items are checked
        if ($('.checkbox:checked').length == $('.checkbox').length ){
            $("#select_all")[0].checked = true; //change "select all" checked status to true
        }
    });






    $(document).ready(function () {
        

        $("#RecoverSelected").click(function () {
            $("#multiple").attr('action', 'recover_selected.php');
            $("#multiple").submit();
        });




        $("#DeleteSelected").click(function () {
            $("#multiple").attr('action', 'delete_selected.php');
            $("#multiple").submit();
        });


    });









    function doConfirm() {

        var result = confirm("Are you sure you want to delete?");

        return result;


    }

    $(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    });




</script>


<!-- required for search, block 5 of 5 start -->
<script>

    $(function() {
        var availableTags = [

            <?php
            echo $comma_separated_keywords;
            ?>
        ];

        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
<!-- required for search, block5 of 5 end -->

</body>
</html>